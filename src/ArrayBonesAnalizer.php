<?php
namespace Xaamin\ArrayAnalizer;

use Xaamin\ArrayAnalizer\Contracts\ArrayAnalizerContract;

class ArrayBonesAnalizer implements ArrayAnalizerContract
{
    protected $keys = [];
    protected $extensions = [];

    public function __construct(array $keys = [])
    {
        $this->keys = $keys;
    }

    public function setKeys(array $keys)
    {
        $this->keys = $keys;
    }

    public function getKeys()
    {
        return $this->keys;
    }

    public function setExtensionNodes(array $extensions)
    {
        $this->extensions = $extensions;

        return $this;
    }

    public function getExtensionNodes()
    {
        return $this->extensions;
    }

    public function clean(array $data, $strict = true)
    {
        $keys = $this->keys;

        $data = $this->order($data, null, $keys);

        return $strict ? $this->cleaning($data, $strict) : $data;
    }

    protected function getValues($data, $key)
    {
        foreach ($data as $index => $value) {
            if (!is_array($index) && string_to_lowercase($index) === string_to_lowercase($key)) {
                return $value;
            }
        }

        return null;
    }

    protected function cleaning(array $data, $strict, $node = null, array $keys = null)
    {
        $keys = $this->getKeysForNode($node, $keys);

        foreach ($data as $key => $value) {
            $isExtensionNode = in_array($node, $this->extensions);

            if (is_array($value)) {
                $values = $this->cleaning($value, $strict, $key, $keys);

                if (is_string($values) || (is_array($values) && !empty($values))) {
                    $data[$key] = $values;
                } elseif ($strict && !$isExtensionNode) {
                    unset($data[$key]);
                }
            } elseif ($strict && is_array($keys) && !in_array($key, $keys)) {
                unset($data[$key]);
            }

            if ($strict && !is_array($value) && ($value === null || trim($value) === '')) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    protected function order(array $data, $node = null, array $keys = null)
    {
        $ordered = [];

        $keys = $this->getKeysForNode($node, $keys);

        foreach ($keys as $index => $value) {
            $key = !is_numeric($index) ? $index : $value;

            if (!is_string($key)) {
                $key = $index;
            }

            if (is_array($value)) {
                $values = $this->getValues($data, $key);

                if (!empty($values)) {
                    if (!array_is_associative($values)) {
                        $tmp = [];

                        foreach ($values as $item) {
                            $tmp[] = $this->order($item, null, $value);
                        }

                        $values = $tmp;
                    } else {
                        $values = $this->order($values, $key, $value);
                    }
                }

                $ordered[$key] = $values;
            } else {
                $value = $this->getValues($data, $key);

                $ordered[$key] = $value;
            }
        }

        return $ordered;
    }

    protected function getKeysForNode($node = null, array $keys = null)
    {
        $keys = $keys ? : $this->keys;

        if ($node) {
            $keys = !is_numeric($node) ? array_get($keys, $node, $keys) : $keys;
        }

        return $keys;
    }

}
