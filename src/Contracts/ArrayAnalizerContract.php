<?php
namespace Xaamin\ArrayAnalizer\Contracts;

interface ArrayAnalizerContract
{
    public function getKeys();

    public function setKeys(array $keys);

    public function setExtensionNodes(array $extensions);

    public function getExtensionNodes();

    public function clean(array $data, $strict = true);
}